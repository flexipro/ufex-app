<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('password_resets')->delete();

        User::create([
            'name' => 'Elias Mamalias',
            'email' => 'mamalias23@gmail.com',
            'password' => bcrypt('123456'),
            'activated' => true
        ]);

        User::create([
            'name' => 'Ben Love',
            'email' => 'benlove@flexi-pro.co.uk',
            'password' => bcrypt('123456'),
            'admin' => true
        ]);
    }
}
