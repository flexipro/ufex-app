<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\User;
class AddAdditionalAdminUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        User::create([
            'name' => 'Jon Forsyth',
            'email' => 'jon@forsythuk.net',
            'password' => bcrypt('123456'),
            'activated' => true,
            'admin'=>true
        ]);

        User::create([
            'name' => 'Matthew Brunt',
            'email' => 'matt@forsythuk.net',
            'password' => bcrypt('123456'),
            'activated' => true,
            'admin'=>true
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        User::where('email','jon@forsythuk.net')->delete();
        User::where('email','matt@forsythuk.net')->delete();
    }
}
