<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produce_photos', function($table) {
            $table->increments('id');
            $table->integer('produce_id')->unsigned();
            $table->foreign('produce_id')->references('id')->on('produces')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->binary('photo');
            $table->boolean('profile', false);
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('produce_photos');
    }
}
