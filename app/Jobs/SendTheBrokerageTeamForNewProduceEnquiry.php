<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\User;
use App\Produce;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendTheBrokerageTeamForNewProduceEnquiry extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $user;
    protected $produce;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Produce $produce, User $user)
    {
        $this->user = $user;
        $this->produce = $produce;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $mailer->send('emails.user-new-produce-request', ['user' => $this->user, 'produce'=>$this->produce], function ($m) {
            $m->to('matt@forsythuk.net');
        });
    }
}
