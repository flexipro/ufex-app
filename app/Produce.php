<?php

namespace App;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Produce extends Model
{
	use Sortable, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'produces';

    protected $sortable = ['id',
                           'name',
                           'customer',
                           'type',
                           'description',
                           'price',
                           'per',
                           'created_at',
                           'updated_at'];


    protected function customerSortable($query, $order) {
        return $query->join('users', 'produces.user_id', '=', 'users.id')
            ->orderBy('users.name', $order)
            ->select('produces.*');
    }

    private function queryOrderBuilder($query, array $a)
    {
        $model = $this;

        $order = array_get($a, 'order', 'asc');
        if (!in_array($order, ['asc', 'desc'])) {
            $order = 'asc';
        }

        $sort = array_get($a, 'sort', null);
        if (!is_null($sort)) {
            if ($oneToOneSort = $this->getOneToOneSortOrNull($sort)) {
                $relationName = $oneToOneSort[0];
                $sort = $oneToOneSort[1];

                try {
                    $relation = $query->getRelation($relationName);
                    $query = $this->queryJoinBuilder($query, $relation);
                } catch (BadMethodCallException $e) {
                    throw new ColumnSortableException($relationName, 1, $e);
                } catch (ErrorException $e) {
                    throw new ColumnSortableException($relationName, 2, $e);
                }

                $model = $relation->getRelated();
            }

            if ($this->columnExists($model, $sort)) {
            	if (method_exists($this, camel_case($sort) . 'Sortable')) {
				    return call_user_func_array(array($this, camel_case($sort) . 'Sortable'), array($query, $order));
				} else {
				    return $query->orderBy($sort, $order);
				}
            }
        }

        return $query;

    }

    public function customer() {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function photos() {
        return $this->hasMany('App\ProducePhoto', 'produce_id');
    }

    public function toArray() {
        $arr = parent::toArray();
        $photos = $this->find($arr['id'])->photos()->orderBy('profile', 'desc')->orderBy('id')->get()->toArray();
        $photos_array = [];
        foreach($photos as $photo) {
            $photos_array[] = [
                'resized'=>$photo['photo_base64_resized'],
                'original'=>$photo['photo_base64_original']
            ];
        }
        $arr['photos'] = $photos_array;
        //$arr['photo'] = (string) \Image::make(storage_path('app/images/'.$arr['photo']))->encode('data-url');
        return $arr;
    }
}
