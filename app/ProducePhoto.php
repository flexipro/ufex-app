<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProducePhoto extends Model
{
    protected $table = 'produce_photos';

    public function produce() {
    	return $this->belongsTo('App\Produce', 'produce_id');
    }

    public function toArray() {
        $arr = parent::toArray();
        $arr['photo_base64_resized'] = (string) \Image::make(storage_path('app/images/'.$arr['photo']))->fit(400, 300, function($c) { })->encode('data-url');
        $arr['photo_base64_original'] = (string) \Image::make(storage_path('app/images/'.$arr['photo']))->encode('data-url');
        return $arr;
    }
}
