<?php

namespace App\Ufex\Repositories;

interface UserRepositoryInterface {

	public function all();

	public function find($id);

	public function register($data);

	public function login($data);

	public function attemptActivate($id, $code);

}