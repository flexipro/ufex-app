<?php

namespace App\Ufex\Repositories;

use Auth;
use Carbon\Carbon;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserRepository implements UserRepositoryInterface {

	public function all() {
		return User::all();
	}

	public function find($id) {
		return User::find($id);
	}

	public function register($data) {

		$user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'activation_code' => str_random(30)
        ]);

        return $user;
	}

	public function login($data) {

		try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($data)) {
                throw new \Symfony\Component\HttpKernel\Exception\HttpException(401, 'Invalid Credentials');
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            throw new \Symfony\Component\HttpKernel\Exception\HttpException(500, 'could not create token');
        }

        $user = User::find(Auth::user()->id);

        if(!$user->activated) {
        	//throw new \Symfony\Component\HttpKernel\Exception\HttpException(401, 'User is not activated');
        }

        $payload = JWTAuth::getPayload($token);

        return ['token'=>$token, 'user'=>$user, 'items'=>$user->items()->get()->toArray(), 'payload'=>$payload->toArray()];

	}

	public function attemptActivate($id, $code) {

		$user = $this->find($id);

		if(!$user) {
    		return ['activated'=>false, 'message'=>'User not found'];
    	}

    	if($user->activated) {
    		return ['activated'=>false, 'message'=>'User already activated'];
    	}

    	if($user->activation_code!=$code) {
    		return ['activated'=>false, 'message'=>'Invalid Activation code'];
    	}

    	$user->activated_at = Carbon::now();
    	$user->activated = 1;
    	$user->activation_code = null;
    	$user->save();

    	return ['activated'=>true, 'message'=>'Thank you for activating your account. You can now access all the features of the UFEX app.', 'user'=>$user];

	}

}