<?php

namespace App\Http\Controllers;

use App\Produce;
use App\User;
use App\ProducePhoto;
use App\ProduceRequest;
use DB;
use JWTAuth;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Jobs\SendTheBrokerageTeamForNewListing;
use App\Jobs\SendTheBrokerageTeamForUpdatedListing;
use App\Jobs\SendTheBrokerageTeamForNewProduceEnquiry;
use App\Jobs\SendTheBrokerageTeamForDeletedListing;

class ProducesController extends Controller
{
	public function lists() {

		$user = JWTAuth::parseToken()->authenticate();
		$lists = [];
		
		if($user->activated) {
			$lists = Produce::orderBy('created_at', 'desc')->with('customer')->whereHas('customer', function($q) {
				$q->where('activated', 1);
			})->get();
		}

		$user->activated = $user->activated ? true:false;
		
		return response()->json(compact('lists', 'user'));
	}

	public function produceRequest(Request $request) {

		$user = JWTAuth::parseToken()->authenticate();

		$produce = Produce::find($request->get('produceID'));

		if($produce) {

			$produceRequest = new ProduceRequest;
			$produceRequest->user_id = $user->id;
			$produceRequest->produce_id = $produce->id;
			$produceRequest->save();

			$job = (new SendTheBrokerageTeamForNewProduceEnquiry($produce, $user))->delay(5);

			$this->dispatch($job);
		}

		return response()->json(['data'=>'ok']);
	}

	public function myProduces(Request $request) {
		$user = JWTAuth::parseToken()->authenticate();
		return ['items'=>$user->items()->get()->toArray()];
	}

    public function sync(Request $request) {

    	$user = JWTAuth::parseToken()->authenticate();

    	$items = $request->get('items');

    	//DB::transaction(function() use($items, $user) {
	    	foreach($items as $item) {
	    		if($item['id']) {
	    			$produce = Produce::find($item['id']);
	    		} else {
	    			$produce = new Produce;
	    		}

	    		//prevent duplicates
	    		$check = Produce::where('user_id', $user->id)
	    				->where('name', $item['name'])
	    				->where('type', $item['type'])
	    				->where('description', $item['description'])
	    				->where('price', $item['price'])
	    				->where('per', $item['per'])
	    				->get();

	    		if(($check->count()==0 || isset($item['photo_changed'])) && !isset($item['deleted'])) {

	    			$produce->user_id = $user->id;
		    		$produce->name = $item['name'];
		    		//$produce->photo = $item['photo'];
		    		$produce->type = $item['type'];
		    		$produce->description = $item['description'];
		    		$produce->price = $item['price'];
		    		$produce->per = $item['per'];
		    		$produce->created_at = $item['created_at'];
		    		$produce->updated_at = $item['updated_at'];
		    		$produce->save();

		    		//delete the photos first
		    		ProducePhoto::where('produce_id', $produce->id)->delete();

		    		//save photo
		    		$ctr = 1;
		    		foreach($item['photos'] as $photo) {
		    			if($photo!='') {
		    				//dd($photo);
		    				$photo_name = $produce->id . '_' . $ctr . '.jpg';
			    			$image = \Image::make($photo['original']);

				    		$image->save(storage_path('app/images/' . $photo_name));

				    		$newPhoto = new ProducePhoto;
				    		$newPhoto->produce_id = $produce->id;
				    		$newPhoto->photo = $photo_name;
				    		$newPhoto->save();

				    		$ctr++;
		    			}
		    		}
		    		


		    		if($item['id']) {
		    			$job = (new SendTheBrokerageTeamForUpdatedListing($produce, $user))->delay(5);
		    		} else {
		    			$job = (new SendTheBrokerageTeamForNewListing($produce, $user))->delay(5);
		    		}

		    		$this->dispatch($job);
	    		} elseif(isset($item['deleted'])) {
	    			$job = (new SendTheBrokerageTeamForDeletedListing($user))->delay(5);
	    			$produce->delete();
	    			$this->dispatch($job);
	    		}
	    		
	    	}
	    //});

	    $userItems = User::find($user->id)->items()->get();

    	return response()->json(compact('userItems'));

    }
}
