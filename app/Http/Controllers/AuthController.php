<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Validator;
use JWTAuth;
use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Jobs\SendUserAWelcomeEmail;
use App\Jobs\SendTheBrokerageTeamThatNewUserSignedUp;
use App\Ufex\Repositories\UserRepositoryInterface;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins, Helpers;

    private $user;

    public function __construct(UserRepositoryInterface $user)
	{
		$this->user = $user;
	}

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */

    public function authenticate(Request $request)
    {	
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        $response = $this->user->login($credentials);

        // all good so return the token
        return response()->json($response);
    }


    public function store(Request $request) {

    	$rules = [
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', 'min:6'],
            'name'	=> ['required', 'min:3']
        ];

        $payload = app('request')->only('email', 'password', 'name');

        $validator = app('validator')->make($payload, $rules);

        if ($validator->fails()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not create new user.', $validator->errors());
        }


        $user = $this->user->register($payload);

        $job = (new SendUserAWelcomeEmail($user));

        $this->dispatch($job);

        $response = [
        	'message' => 'Thank you for registering. We have sent you an email to confirm and activate your account.'
        ];

        return response()->json($response);

    }

    public function activate(Request $request, $userId, $code) {

    	$attempt = $this->user->attemptActivate($userId, $code);

    	if($attempt['activated']) {
    		$job = (new SendTheBrokerageTeamThatNewUserSignedUp($attempt['user']))->delay(5);
        	$this->dispatch($job);
    	}

        $message = $attempt['message'];

    	return view('activation', compact('message'));

    }

    public function resendConfirmationEmail(Request $request) {
        $user = JWTAuth::parseToken()->authenticate();

        if(!$user->activated) {

            $job = (new SendUserAWelcomeEmail($user));

            $this->dispatch($job);

            $message = 'We have sent you an email to confirm and activate your account.';

        } else {

            $message = 'You have already activated your account.';
            
        }

        return response()->json(compact('user', 'message'));
    }
}
