<?php

namespace App\Http\Controllers;

use App\Enquiry;
use App\User;
use DB;
use JWTAuth;
use Dingo\Api\Routing\Helpers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Jobs\SendTheBrokerageTeamForNewEnquiry;

class EnquiriesController extends Controller
{
    public function send(Request $request) {
    	$user = JWTAuth::parseToken()->authenticate();

    	$enquiry = new Enquiry;
    	$enquiry->user_id = $user->id;
    	$enquiry->enquiry = $request->get('description');
    	$enquiry->save();

    	$job = (new SendTheBrokerageTeamForNewEnquiry($user))->delay(5);
    	$this->dispatch($job);

    	return response()->json(compact('enquiry'));
    }
}
