<?php

namespace App\Http\Controllers\Web;

use App\Enquiry;
use App\ProduceRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class RequestsController extends Controller
{
    protected $enquiry;
	protected $produceRequest;

	public function __construct(Enquiry $enquiry, ProduceRequest $produceRequest) {
        $this->enquiry = $enquiry;
		$this->produceRequest = $produceRequest;
	}

    public function index() {

    	$enquiries = $this->enquiry->sortable()->paginate(10);

    	return view('requests', compact('enquiries'));
    }

    public function produce() {

        $requests = $this->produceRequest->sortable()->paginate(10);

        return view('produce-requests', compact('requests'));
    }

    public function delete(Request $request, $id) {

        $enquiry = $this->enquiry->find($id);

        if(!$enquiry) {
            return back()->withError('Buy request not found');
        }

        $enquiry->delete(); // :(

        return back()->withSuccess('Buy request successfully deleted');
    }

    public function deleteProduce(Request $request, $id) {

        $requestDelete = $this->produceRequest->find($id);

        if(!$requestDelete) {
            return back()->withError('Request not found');
        }

        $requestDelete->delete(); // :(

        return back()->withSuccess('Produce request successfully deleted');
    }
}
