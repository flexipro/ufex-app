<?php

namespace App\Http\Controllers\Web;

use App\Produce;
use App\ProducePhoto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class ProduceController extends Controller
{
	protected $produce;

	public function __construct(Produce $produce) {
		$this->produce = $produce;
	}

    public function index() {

    	$produces = $this->produce->with('customer')->sortable(['name'])->paginate(10);

        //dd($produces);

    	return view('produces', compact('produces'));

    }

    public function update(Request $request) {

    	$produce = $this->produce->find($request->get('id'));
    	if(!$produce) {
    		return back()->withError('Produce not found');
    	}

    	$produce->name = $request->get('name');
    	$produce->type = $request->get('type');
    	$produce->description = $request->get('description');
        $produce->price = $request->get('price');
    	$produce->per = $request->get('per');

    	$produce->save();

        ProducePhoto::where('profile', 1)->update(['profile'=>0]);
        ProducePhoto::where('id', $request->get('profile'))->update(['profile'=>1]);

    	return back()->withSuccess('Produce successfully updated');

    }

    public function delete(Request $request, $id) {
        $produce = $this->produce->find($id);

        if(!$produce) {
            return back()->withError('Produce not found');
        }

        $produce->delete(); // :(

        return back()->withSuccess('Produce successfully deleted');
    }

    public function deletePhoto(Request $request, $id) {

        $photo = ProducePhoto::where('id', $id)->get();

        if($photo->count()) {

            $photos = ProducePhoto::where('produce_id', $photo->first()->produce_id)->get();

            if($photos->count()>1) {
                $request->session()->flash('success', 'Photo successfully deleted!');
                $photo->first()->delete();
            } elseif($photo->count()==1) {
                return '1';
            }
        } else {
            return '0';
        }

        return 'success';
    }
}
