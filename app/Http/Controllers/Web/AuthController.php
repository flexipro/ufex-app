<?php

namespace App\Http\Controllers\Web;

use App\User;
use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Jobs\SendUserAWelcomeEmail;
use App\Jobs\SendTheBrokerageTeamThatNewUserSignedUp;
use App\Ufex\Repositories\UserRepositoryInterface;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    private $user;

    public function __construct(UserRepositoryInterface $user)
	{
		$this->user = $user;
	}

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */

    public function authenticate(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');	
        if (Auth::attempt(['email' => $email, 'password' => $password, 'admin'=>1])) {
            // Authentication passed...
            return redirect('/');
        }

        return redirect()->back()->withError('Invalid Login');
    }

    public function login() {
        return view('login');
    }

    public function logout() {
        Auth::logout();
        return redirect('login');
    }
}
