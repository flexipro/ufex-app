<?php

namespace App\Http\Controllers\Web;

use App\User;
use App\Produce;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class CustomersController extends Controller
{
	protected $user;
	protected $produce;

	public function __construct(User $user, Produce $produce) {
		$this->user = $user;
		$this->produce = $produce;
	}

    public function index() {

    	//$users = $this->user->sortable(['name'])->where('admin', 0)->paginate(10); without admin

        $users = $this->user->sortable(['name'])->paginate(10); //with admin

    	return view('customers', compact('users'));
    }

    public function update(Request $request) {

    	$user = $this->user->find($request->get('id'));
    	if(!$user) {
    		return back()->withError('Customer not found');
    	}

    	$user->name = $request->get('name');
    	$user->email = $request->get('email');
    	$user->company = $request->get('company');
    	$user->telephone = $request->get('telephone');
    	$user->mobile = $request->get('mobile');
        if($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
        }
    	$user->save();

    	return back()->withSuccess('Customer successfully updated');

    }

    public function delete(Request $request, $id) {

        $user = $this->user->find($id);

        if(!$user) {
            return back()->withError('Customer not found');
        }

        if($user->admin==1) {
            return back()->withError('You cannot delete admin users');
        }

        $user->delete(); // :(

        return back()->withSuccess('Customer successfully deleted');
    }


    public function produce(Request $request, $id) {

    	$customer = $this->user->find($id);
    	$produces = $this->produce->where('user_id', $id)->with('customer')->sortable(['name'])->paginate(10);
    	return view('produces', compact('produces', 'customer'));

    }
}
