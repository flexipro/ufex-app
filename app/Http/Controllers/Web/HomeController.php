<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Produce;
use App\Http\Controllers\Controller;
use App\Jobs\SendTheBrokerageTeamForNewEnquiry;

class HomeController extends Controller
{
    public function index() {
        return view('welcome');
    }

    public function image(Request $request, $id) {
    	return '<img style="border:1px solid black" src="'.Produce::find($id)->photo.'" width="100" height="100">';
    }
}
