<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {

	$api->group(['middleware' => ['api.auth']], function () use($api) {
		$api->get('check-auth', function() {
			$user = app('Dingo\Api\Auth\Auth')->user();
        	return $user;
		});
		$api->get('produces', 'App\Http\Controllers\ProducesController@lists');
		$api->get('my-produces', 'App\Http\Controllers\ProducesController@myProduces');
		$api->post('sync-items', 'App\Http\Controllers\ProducesController@sync');
		$api->post('produce-request', 'App\Http\Controllers\ProducesController@produceRequest');
		$api->post('enquiry', 'App\Http\Controllers\EnquiriesController@send');
		$api->get('resend-confirmation-email', 'App\Http\Controllers\AuthController@resendConfirmationEmail');
	});

	$api->post('auth', 'App\Http\Controllers\AuthController@authenticate');
	$api->post('register', 'App\Http\Controllers\AuthController@store');
	$api->get('activate/{userId}/code/{code}', 'App\Http\Controllers\AuthController@activate');

	$api->get('check', function() {
		return 'Welcome to UFEX api v1';
	});

	$api->get('welcome', function() {
		return view('greeting');
	});

});

Route::group(['middleware' => ['web']], function () {

	Route::get('/login', ['as'=>'login', 'uses'=>'\App\Http\Controllers\Web\AuthController@login']);
	Route::post('/login', ['as'=>'login', 'uses'=>'\App\Http\Controllers\Web\AuthController@authenticate']);

	Route::group(['middleware' => ['auth']], function () {
		Route::get('/', '\App\Http\Controllers\Web\HomeController@index');
		Route::get('/logout', ['as'=>'logout', 'uses'=>'\App\Http\Controllers\Web\AuthController@logout']);

		Route::get('/customers', ['as'=>'customers.index', 'uses'=>'\App\Http\Controllers\Web\CustomersController@index']);
		Route::put('/customers', ['as'=>'customers.update', 'uses'=>'\App\Http\Controllers\Web\CustomersController@update']);
		Route::get('/customers/{id}/produce', ['as'=>'customers.produce', 'uses'=>'\App\Http\Controllers\Web\CustomersController@produce']);
		Route::delete('/customers/{id}', ['as'=>'customers.delete', 'uses'=>'\App\Http\Controllers\Web\CustomersController@delete']);
		Route::get('/produces', ['as'=>'produces.index', 'uses'=>'\App\Http\Controllers\Web\ProduceController@index']);
		Route::put('/produces', ['as'=>'produces.update', 'uses'=>'\App\Http\Controllers\Web\ProduceController@update']);
		Route::delete('/produces/{id}', ['as'=>'produces.delete', 'uses'=>'\App\Http\Controllers\Web\ProduceController@delete']);
		Route::get('/requests/buy', ['as'=>'requests.buy', 'uses'=>'\App\Http\Controllers\Web\RequestsController@index']);
		Route::get('/requests/produce', ['as'=>'requests.produce', 'uses'=>'\App\Http\Controllers\Web\RequestsController@produce']);
		Route::delete('/requests/{id}', ['as'=>'requests.delete', 'uses'=>'\App\Http\Controllers\Web\RequestsController@delete']);
		Route::delete('/produce-requests/{id}', ['as'=>'requests.delete_produce', 'uses'=>'\App\Http\Controllers\Web\RequestsController@deleteProduce']);
		Route::get('/delete-photo/{id}', ['as'=>'produce.delete_photo', 'uses'=>'\App\Http\Controllers\Web\ProduceController@deletePhoto']);
	});


	Route::get('image/{id}', '\App\Http\Controllers\Web\HomeController@image');

	Route::get('images/{filename}/{width?}', function($filename, $width=null) {
		if($width) {
			return Image::make(storage_path('app/images/'.$filename))
					->resize($width, null, function($c) {
						$c->aspectRatio();
					})
					->response();
		}
		return Image::make(storage_path('app/images/'.$filename))->response();
	});
	
	Route::get('testenv', function() {
	
		echo "Site URL: " . env('SITE_URL') . "<br>";
		echo "EMAIL: " . env('BROKERAGE_EMAIL') . "<br>";
		echo "API_URL: " . env('API_URL') . "<br>";
		echo "API_DOMAIN: " . env('API_DOMAIN') . "<br>";
		echo "API_PREFIX: " . env('API_PREFIX') . "<br>";
	
	});
	
});
