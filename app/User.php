<?php

namespace App;

use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Sortable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'activation_code', 'company', 'telephone', 'mobile', 'activated', 'admin'
    ];

    protected $sortable = ['id',
                           'name',
                           'email',
                           'company',
                           'telephone',
                           'mobile',
                           'created_at',
                           'updated_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'admin', 'activation_code', 'activated_at'
    ];

    public function items() {
        return $this->hasMany('App\Produce');
    }

    public function enquiries() {
        return $this->hasMany('App\Enquiry');
    }
}
