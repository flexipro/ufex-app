<?php

namespace App;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;

class ProduceRequest extends Model
{
	use Sortable;
    protected $table = 'produce_requests';

    protected $sortable = ['id',
                           'name',
                           'email',
                           'company',
                           'telephone',
                           'mobile',
                           'produce',
                           'created_at',
                           'updated_at'];

    private function queryOrderBuilder($query, array $a)
    {
        $model = $this;

        $order = array_get($a, 'order', 'asc');
        if (!in_array($order, ['asc', 'desc'])) {
            $order = 'asc';
        }

        $sort = array_get($a, 'sort', null);
        if (!is_null($sort)) {
            if ($oneToOneSort = $this->getOneToOneSortOrNull($sort)) {
                $relationName = $oneToOneSort[0];
                $sort = $oneToOneSort[1];

                try {
                    $relation = $query->getRelation($relationName);
                    $query = $this->queryJoinBuilder($query, $relation);
                } catch (BadMethodCallException $e) {
                    throw new ColumnSortableException($relationName, 1, $e);
                } catch (ErrorException $e) {
                    throw new ColumnSortableException($relationName, 2, $e);
                }

                $model = $relation->getRelated();
            }

            if ($this->columnExists($model, $sort)) {
            	if (method_exists($this, camel_case($sort) . 'Sortable')) {
				    return call_user_func_array(array($this, camel_case($sort) . 'Sortable'), array($query, $order));
				} else {
				    return $query->orderBy($sort, $order);
				}
            }
        }

        return $query;

    }
    
    protected function nameSortable($query, $order) {
        return $query->join('users', 'produce_requests.user_id', '=', 'users.id')
            ->orderBy('users.name', $order)
            ->select('produce_requests.*');
    }

    protected function emailSortable($query, $order) {
        return $query->join('users', 'produce_requests.user_id', '=', 'users.id')
            ->orderBy('users.email', $order)
            ->select('produce_requests.*');
    }

    protected function companySortable($query, $order) {
        return $query->join('users', 'produce_requests.user_id', '=', 'users.id')
            ->orderBy('users.company', $order)
            ->select('produce_requests.*');
    }

    protected function telephoneSortable($query, $order) {
        return $query->join('users', 'produce_requests.user_id', '=', 'users.id')
            ->orderBy('users.telephone', $order)
            ->select('produce_requests.*');
    }

    protected function mobileSortable($query, $order) {
        return $query->join('users', 'produce_requests.user_id', '=', 'users.id')
            ->orderBy('users.mobile', $order)
            ->select('produce_requests.*');
    }

    protected function produceSortable($query, $order) {
        return $query->join('produces', 'produce_requests.produce_id', '=', 'produces.id')
            ->orderBy('produces.name', $order)
            ->select('produce_requests.*');
    }

    public function produce() {
    	return $this->belongsTo('App\Produce', 'produce_id');
    }

    public function customer() {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
