<!DOCTYPE html>
<html>
    <head>
        <title>UFEX</title>
        <link rel="stylesheet" href="{{ asset('semantic/dist/semantic.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" />
        <style>
        .container {
            margin-top:20px;
            height:100%;
        }

        #photo_container {
            min-height: 200px;
        }

        .image_radio {
            float:left;
            margin-right: 15px;
            position: relative;
        }

        .image_radio img {
            margin-left: 15px;
        }

        .image_radio .icon {
            right: 0;
            position: absolute;
            color: red;
            font-size: 25px;
            cursor: pointer;
        }

        .image_radio input {
            position: absolute;
        }
        </style>
        @yield('on-page-styles')
    </head>
    <body>
        
        <div class="ui container">

            @if(Auth::check())
                <div class="ui secondary pointing menu">
                    <a href="/" class="item {{ Request::is('/') ? 'active':'' }}">
                        Home
                    </a>
                    <a href="{{ route('customers.index') }}" class="item {{ Request::is('customers*') ? 'active':'' }}">
                        Customers
                    </a>
                    <a href="{{ route('produces.index') }}" class="item {{ Request::is('produces*') ? 'active':'' }}">
                        Produce
                    </a>
                    <div class="ui dropdown item">
                        Requests
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <a class="item" href="{{ route('requests.buy') }}">Buy Requests</a>
                            <a class="item" href="{{ route('requests.produce') }}">Produce Requests</a>
                        </div>
                    </div>
                    <div class="right menu">
                        <a href="{{ route('logout') }}" class="ui item">
                            Logout
                        </a>
                    </div>
                </div>

                @if(Session::has('success'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            {{ Session::get('success') }}
                        </div>
                    </div>
                @endif

                @if(Session::has('error'))
                    <div class="ui negative message">
                        <i class="close icon"></i>
                        <div class="header">
                            {{ Session::get('error') }}
                        </div>
                    </div>
                @endif
            @endif

            @yield('content')

        </div>

        <script type="text/javascript" src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('semantic/dist/semantic.min.js') }}"></script>

        <script type="text/javascript">
            $('.message .close')
              .on('click', function() {
                $(this)
                  .closest('.message')
                  .transition('fade')
                ;
              })
            ;

            $('.ui.radio.checkbox')
              .checkbox()
            ;

            $('.ui.dropdown')
              .dropdown()
            ;

            var message;
            $(function(){
                $('[data-method]').append(function(){
                    message = $(this).data('message');
                    return "\n"+
                    "<form action='"+$(this).attr('href')+"' method='POST' style='display:none'>\n"+
                    "   <input type='hidden' name='_token' value='"+$(this).attr('data-token')+"'>\n"+
                    "   <input type='hidden' name='_method' value='"+$(this).attr('data-method')+"'>\n"+
                    "</form>\n"
                })
                .removeAttr('href')
                .attr('style','cursor:pointer;')
                .attr('onclick','if(confirm("'+message+'"))$(this).find("form").submit();');
            });

            $(function(){
                $(document.body).on('click', '.image_radio .remove' ,function(){
                    if(confirm("Are you sure you want to delete that photo?")) {
                        $.get( "/delete-photo/" + $(this).data('id'), function( data ) {
                            if(data=='1') {
                                alert('You cannot delete that photo because this produce has only 1 photo');
                            } else if(data=='0') {
                                alert('Photo not found');
                            } else {
                                location.reload();
                            }
                        });
                    } else {
                        //
                    }
                });
            });
        </script>

        @yield('on-page-scripts')
    </body>
</html>
