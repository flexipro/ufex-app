@extends('layouts.default')

@section('on-page-styles')
    <style type="text/css">
        body .grid {
          height: 100%;
        }
    </style>
@stop

@section('content')
	@if(!Request::is('customers*'))
		<h1>Produce</h1>
	@else
		<h1>Produce for {{ $customer->name }}</h1>
	@endif
	<table class="ui celled table">
		<thead>
			<tr>
				<th>@sortablelink ('id', 'ID')</th>
				<th width="80px">Photo</th>
				@if(!Request::is('customers*'))
					<th>@sortablelink ('customer', 'Customer')</th>
				@endif
				<th>@sortablelink ('name', 'Name')</th>
				<th>@sortablelink ('type', 'Type')</th>
				<th width="300px">@sortablelink ('description', 'Description')</th>
				<th class="right aligned">@sortablelink ('price', 'Price')</th>
				<th>@sortablelink ('per', 'Per')</th>
				<th>@sortablelink ('created_at', 'Date and time')</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		@foreach($produces as $produce)
			<?php
			$images_array = [];
			$photos = $produce->photos()->orderBy('id')->get();

			foreach($photos as $photo) {
				$images_array[] = ["id"=>$photo->id,"photo"=>$photo->photo, "profile"=> $photo->profile];
			}

			$images_json = json_encode($images_array);

			?>
			<tr>
				<td>{{ $produce->id }}</td>
				<td><img src="/images/{{ $produce->photos()->orderBy('profile', 'desc')->orderBy('id')->first()->photo }}/80" /></td>
				@if(!Request::is('customers*'))
					<td><a href="javascript:;" data-id="{{ $produce->customer->id }}" 
						data-name="{{ $produce->customer->name }}" 
						data-email="{{ $produce->customer->email }}" 
						data-company="{{ $produce->customer->company }}" 
						data-telephone="{{ $produce->customer->telephone }}" 
						data-mobile="{{ $produce->customer->mobile }}" 
						class="edit_customer">{{ $produce->customer->name }}</a></td>
				@endif
				<td>{{ $produce->name }}</td>
				<td>{{ $produce->type }}</td>
				<td>{{ $produce->description }}</td>
				<td class="right aligned">{{ $produce->price }}</td>
				<td>{{ $produce->per }}</td>
				<td>{{ $produce->created_at->format('d/m/Y H:i') }}</td>
				<td>
					<div class="ui buttons mini">
						<button 
						class="ui blue button mini edit" 
						data-id="{{ $produce->id }}" 
						data-images='{{ $images_json }}'
						data-name="{{ $produce->name }}" 
						data-type="{{ $produce->type }}" 
						data-description="{{ $produce->description }}" 
						data-price="{{ $produce->price }}"
						data-per="{{ $produce->per }}">Edit</button>
						<div class="or"></div>
						<a class="ui red button mini" href="{{ route('produces.delete', [$produce->id]) }}" data-method="delete" data-token="{{ csrf_token() }}" data-message="Are you sure you want to delete?">Delete</a>
					</div>
				</td>
			</tr>
		@endforeach

		@if($produces->count()==0)
			<tr><td colspan="8">No Data</td></tr>
		@endif
		</tbody>
	</table>
    
    @include('pagination.default', ['paginator' => $produces])

    @include('modals.edit-produce')
    @include('modals.edit-customer')
@stop

@section('on-page-scripts')
	<script type="text/javascript">
		$(".edit").on("click", function() {
			$this = $(this);
			$("input[name='id']").val($this.data('id'));
			$("input[name='name']").val($this.data('name'));

			if($this.data('type')=='Fruit') {
				$("input#fruit").prop("checked", true);
			} else {
				$("input#veg").prop("checked", true);
			}

			var images = $this.data('images');
			$("#photo_container").html("");
			console.log(images);
			var htmlInsert = '';
			var hasProfile = false;
			$.each(images, function(key, value) {
				if(value.profile==1) {
					hasProfile = true;
					htmlInsert += '<div class="image_radio"><i class="remove icon" data-id="'+value.id+'"></i><input type="radio" checked name="profile" id="'+value.id+'" value="'+value.id+'"><label for="'+value.id+'"><img src="/images/'+value.photo+'/180" style="width:180px;height:200px" /></label></div>';
				} else {
					htmlInsert += '<div class="image_radio"><i class="remove icon" data-id="'+value.id+'"></i><input type="radio" name="profile" id="'+value.id+'" value="'+value.id+'"><label for="'+value.id+'"><img src="/images/'+value.photo+'/180" style="width:180px;height:200px" /></label></div>';
				}
			});

			$("#photo_container").append(htmlInsert);

			if(hasProfile==false) {
				var first = $("#photo_container").find(".image_radio").first();

				first.find("input[type='radio']").first().prop("checked", true);
			}

			$("textarea[name='description']").val($this.data('description'));
			$("input[name='price']").val($this.data('price'));
			$("select[name='per']").dropdown('set selected', $this.data('per'));
			$('.ui.modal.produce').modal('show');
		});

		$(".submit").on("click", function() {
			$("form[name='myFormProduce']").submit();
		});

		$(document)
	        .ready(function() {
	          $('#myFormProduce')
	            .form({
	              fields: {
	              	name: {
	                  identifier  : 'name',
	                  rules: [
	                    {
	                      type   : 'empty',
	                      prompt : 'Please enter Name'
	                    },
	                    {
	                      type   : 'length[3]',
	                      prompt : 'Name must be at least 3 characters'
	                    }
	                  ]
	                },
	                price: {
	                  identifier  : 'price',
	                  rules: [
	                    {
	                      type   : 'empty',
	                      prompt : 'Please enter Price'
	                    },
	                    {
	                      type   : 'number',
	                      prompt : 'Price must be valid number'
	                    }
	                  ]
	                },
	                description: {
	                  identifier  : 'description',
	                  rules: [
	                    {
	                      type   : 'empty',
	                      prompt : 'Please enter Description'
	                    },
	                    {
	                      type   : 'length[3]',
	                      prompt : 'Description must be at least 3 characters'
	                    }
	                  ]
	                },
	              }
	            })
	          ;
	        })
	    ;

	    $(".edit_customer").on("click", function() {
			$this = $(this);
			$("input[name='id']").val($this.data('id'));
			$("input[name='name']").val($this.data('name'));
			$("input[name='email']").val($this.data('email'));
			$("input[name='company']").val($this.data('company'));
			$("input[name='telephone']").val($this.data('telephone'));
			$("input[name='mobile']").val($this.data('mobile'));
			$('.ui.modal.customer').modal('show');
		});

		$(".submit").on("click", function() {
			$("form[name='myFormCustomer']").submit();
		});

		$(document)
	        .ready(function() {
	          $('#myFormCustomer')
	            .form({
	              fields: {
	              	name: {
	                  identifier  : 'name',
	                  rules: [
	                    {
	                      type   : 'empty',
	                      prompt : 'Please enter your Name'
	                    },
	                    {
	                      type   : 'length[3]',
	                      prompt : 'Your name must be at least 3 characters'
	                    }
	                  ]
	                },
	                email: {
	                  identifier  : 'email',
	                  rules: [
	                    {
	                      type   : 'empty',
	                      prompt : 'Please enter your e-mail'
	                    },
	                    {
	                      type   : 'email',
	                      prompt : 'Please enter a valid e-mail'
	                    }
	                  ]
	                }
	              }
	            })
	          ;
	        })
	    ;
	</script>
@stop
