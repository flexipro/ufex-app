@extends('layouts.default')

@section('on-page-styles')
    <style type="text/css">
        body .grid {
          height: 100%;
        }
    </style>
@stop

@section('content')
	<h1>Customers</h1>
	<table class="ui celled table">
		<thead>
			<tr>
				<th>@sortablelink ('id', 'ID')</th>
				<th>@sortablelink ('name', 'Name')</th>
				<th>@sortablelink ('email', 'Email')</th>
				<th>@sortablelink ('company', 'Company')</th>
				<th>@sortablelink ('telephone', 'Telephone')</th>
				<th>@sortablelink ('mobile', 'Mobile')</th>
				<th width="250px">Action</th>
			</tr>
		</thead>
		<tbody>
		@foreach($users as $user)
			<tr>
				<td>{{ $user->id }}</td>
				<td>{{ $user->name }}</td>
				<td>{{ $user->email }}</td>
				<td>{{ $user->company }}</td>
				<td>{{ $user->telephone }}</td>
				<td>{{ $user->mobile }}</td>
				<td>
					<div class="ui buttons mini">
					  	<button 
					  	class="ui blue button edit"
					  	data-id="{{ $user->id }}" 
						data-name="{{ $user->name }}" 
						data-email="{{ $user->email }}" 
						data-company="{{ $user->company }}" 
						data-telephone="{{ $user->telephone }}" 
						data-mobile="{{ $user->mobile }}">Edit</button>
					  	<div class="or"></div>
					  	<a href="{{ route('customers.delete', [$user->id]) }}" data-method="delete" data-token="{{ csrf_token() }}" data-message="Are you sure you want to delete?" class="ui red button">Delete</a>
					  	<div class="or"></div>
					  	<a href="{{ route('customers.produce', [$user->id]) }}" class="ui green positive button">Customer's produce</a>
					</div>
				</td>
			</tr>
		@endforeach

		@if($users->count()==0)
			<tr><td colspan="7">No Data</td></tr>
		@endif
		</tbody>
	</table>
    
    @include('pagination.default', ['paginator' => $users])

    @include('modals.edit-customer')
@stop

@section('on-page-scripts')
	<script type="text/javascript">
		$(".edit").on("click", function() {
			$this = $(this);
			$("input[name='id']").val($this.data('id'));
			$("input[name='name']").val($this.data('name'));
			$("input[name='email']").val($this.data('email'));
			$("input[name='company']").val($this.data('company'));
			$("input[name='telephone']").val($this.data('telephone'));
			$("input[name='mobile']").val($this.data('mobile'));
			$('.ui.modal.customer').modal('show');
		});

		$(".submit").on("click", function() {
			$("form[name='myFormCustomer']").submit();
		});

		$(document)
	        .ready(function() {
	          $('.ui.form.customer')
	            .form({
	              fields: {
	              	name: {
	                  identifier  : 'name',
	                  rules: [
	                    {
	                      type   : 'empty',
	                      prompt : 'Please enter your Name'
	                    },
	                    {
	                      type   : 'length[3]',
	                      prompt : 'Your name must be at least 3 characters'
	                    }
	                  ]
	                },
	                email: {
	                  identifier  : 'email',
	                  rules: [
	                    {
	                      type   : 'empty',
	                      prompt : 'Please enter your e-mail'
	                    },
	                    {
	                      type   : 'email',
	                      prompt : 'Please enter a valid e-mail'
	                    }
	                  ]
	                }
	              }
	            })
	          ;
	        })
	    ;
	</script>
@stop
