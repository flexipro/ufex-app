<div class="ui modal produce">
    <div class="header">
        Edit Produce
    </div>
    <div class="content">
        {!! Form::open(['route'=>'produces.update', 'class'=>'ui form produce', 'name'=>'myFormProduce', 'id'=>'myFormProduce', 'method'=>'PUT']) !!}
            <input type="hidden" name="id">
            <div class="four fields">
                <div class="field">
                    <label>Name</label>
                    <input type="text" name="name" placeholder="Name">
                </div>
                <div class="inline fields" style="margin-top:15px;margin-left:15px;">
                    <label for="fruit">Type:</label>
                    <div class="field">
                        <div class="ui radio checkbox">
                            <input type="radio" id="fruit" name="type" value="Fruit" class="hidden">
                            <label>Fruit</label>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui radio checkbox">
                            <input type="radio" id="veg" name="type" value="Veg" class="hidden">
                            <label>Veg</label>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label>Price</label>
                    <input type="text" name="price" placeholder="Price">
                </div>
                <div class="field">
                    <label>Per</label>
                    <select name="per" class="ui fluid dropdown">
                        <option value="box">Per Box</option>
                        <option value="pallet">Per Pallet</option>
                        <option value="kg">Per Kg</option>
                        <option value="bag">Per Bag</option>
                        <option value="other">Other</option>
                    </select>
                </div>
            </div>
            <div class="field">
                <label style="margin-bottom:15px">Profile Photo</label>
                <div id="photo_container"> 
                </div>
            </div>
            <div class="field">
                <label>Description</label>
                <textarea name="description" placeholder="Description"></textarea>
            </div>

        <div class="ui error message"></div>
        {!! Form::close() !!}

    </div>
    <div class="actions">
        <div class="ui deny button">
            Cancel
        </div>
        <div class="ui green right labeled icon button submit">
            Save
            <i class="checkmark icon"></i>
        </div>
    </div>
</div>