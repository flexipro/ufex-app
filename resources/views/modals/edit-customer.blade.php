<div class="ui modal customer">
    <div class="header">
        Edit Customer
    </div>
    <div class="content">
        {!! Form::open(['route'=>'customers.update', 'class'=>'ui form customer', 'name'=>'myFormCustomer', 'id'=>'myFormCustomer', 'method'=>'PUT']) !!}
            <input type="hidden" name="id">
            <div class="two fields">
                <div class="field">
                    <label>Name</label>
                    <input type="text" name="name" placeholder="Name">
                </div>
                <div class="field">
                    <label>Company</label>
                    <input type="text" name="company" placeholder="Company">
                </div>
            </div>
            <div class="three fields">
                <div class="field">
                    <label>Email</label>
                    <input type="email" name="email" placeholder="E-mail">
                </div>
                <div class="field">
                    <label>Telephone</label>
                    <input type="text" name="telephone" placeholder="Telephone">
                </div>
                <div class="field">
                    <label>Mobile</label>
                    <input type="text" name="mobile" placeholder="Mobile">
                </div>
            </div>

            <div class="two fields">
                <div class="field">
                    <label>Password (please leave this blank if you don't want to change)</label>
                    <input type="password" name="password" placeholder="Password">
                </div>
            </div>
        <div class="ui error message"></div>
        {!! Form::close() !!}

    </div>
    <div class="actions">
        <div class="ui deny button">
            Cancel
        </div>
        <div class="ui green right labeled icon button submit">
            Save
            <i class="checkmark icon"></i>
        </div>
    </div>
</div>