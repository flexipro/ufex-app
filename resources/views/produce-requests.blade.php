@extends('layouts.default')

@section('on-page-styles')
    <style type="text/css">
        body .grid {
          height: 100%;
        }
    </style>
@stop

@section('content')
	<h1>Produce Requests</h1>
	<table class="ui celled table">
		<thead>
			<tr>
				<th>@sortablelink ('id', 'ID')</th>
				<th>@sortablelink ('name', 'Name')</th>
				<th>@sortablelink ('email', 'Email')</th>
				<th>@sortablelink ('company', 'Company')</th>
				<th>@sortablelink ('telephone', 'Telephone')</th>
				<th>@sortablelink ('mobile', 'Mobile')</th>
				<th>@sortablelink ('produce', 'Request')</th>
				<th width="150px">Action</th>
			</tr>
		</thead>
		<tbody>
		@foreach($requests as $request)
			<?php
			$images_array = [];
			$photos = $request->produce->photos()->orderBy('id')->get();

			foreach($photos as $photo) {
				$images_array[] = ["id"=>$photo->id,"photo"=>$photo->photo, "profile"=> $photo->profile];
			}

			$images_json = json_encode($images_array);

			?>
			<tr>
				<td>{{ $request->id }}</td>
				<td>{{ $request->customer->name }}</td>
				<td>{{ $request->customer->email }}</td>
				<td>{{ $request->customer->company }}</td>
				<td>{{ $request->customer->telephone }}</td>
				<td>{{ $request->customer->mobile }}</td>
				<td><a href="javascript:;" 
					class="edit" 
					data-id="{{ $request->produce->id }}" 
					data-images='{{ $images_json }}'
					data-name="{{ $request->produce->name }}" 
					data-type="{{ $request->produce->type }}" 
					data-description="{{ $request->produce->description }}" 
					data-price="{{ $request->produce->price }}"
					data-per="{{ $request->produce->per }}">{{ $request->produce->name }} ({{ $request->produce->id }})</a></td>
				<td>
					<a href="{{ route('requests.delete_produce', [$request->id]) }}" data-method="delete" data-token="{{ csrf_token() }}" data-message="Are you sure you want to delete?" class="ui red button mini">Delete</a>
				</td>
			</tr>
		@endforeach

		@if($requests->count()==0)
			<tr><td colspan="7">No Data</td></tr>
		@endif
		</tbody>
	</table>
    
    @include('pagination.default', ['paginator' => $requests])

    @include('modals.edit-produce')

@stop

@section('on-page-scripts')
	<script type="text/javascript">
		$(".edit").on("click", function() {
			$this = $(this);
			$("input[name='id']").val($this.data('id'));
			$("input[name='name']").val($this.data('name'));

			if($this.data('type')=='Fruit') {
				$("input#fruit").prop("checked", true);
			} else {
				$("input#veg").prop("checked", true);
			}

			var images = $this.data('images');
			$("#photo_container").html("");
			console.log(images);
			var htmlInsert = '';
			var hasProfile = false;
			$.each(images, function(key, value) {
				if(value.profile==1) {
					hasProfile = true;
					htmlInsert += '<div class="image_radio"><i class="remove icon" data-id="'+value.id+'"></i><input type="radio" checked name="profile" id="'+value.id+'" value="'+value.id+'"><label for="'+value.id+'"><img src="/images/'+value.photo+'/180" style="width:180px;height:200px" /></label></div>';
				} else {
					htmlInsert += '<div class="image_radio"><i class="remove icon" data-id="'+value.id+'"></i><input type="radio" name="profile" id="'+value.id+'" value="'+value.id+'"><label for="'+value.id+'"><img src="/images/'+value.photo+'/180" style="width:180px;height:200px" /></label></div>';
				}
			});

			$("#photo_container").append(htmlInsert);

			if(hasProfile==false) {
				var first = $("#photo_container").find(".image_radio").first();

				first.find("input[type='radio']").first().prop("checked", true);
			}

			$("textarea[name='description']").val($this.data('description'));
			$("input[name='price']").val($this.data('price'));
			$("select[name='per']").dropdown('set selected', $this.data('per'));
			$('.ui.modal.produce').modal('show');
		});

		$(".submit").on("click", function() {
			$("form[name='myFormProduce']").submit();
		});

		$(document)
	        .ready(function() {
	          $('#myFormProduce')
	            .form({
	              fields: {
	              	name: {
	                  identifier  : 'name',
	                  rules: [
	                    {
	                      type   : 'empty',
	                      prompt : 'Please enter Name'
	                    },
	                    {
	                      type   : 'length[3]',
	                      prompt : 'Name must be at least 3 characters'
	                    }
	                  ]
	                },
	                price: {
	                  identifier  : 'price',
	                  rules: [
	                    {
	                      type   : 'empty',
	                      prompt : 'Please enter Price'
	                    },
	                    {
	                      type   : 'number',
	                      prompt : 'Price must be valid number'
	                    }
	                  ]
	                },
	                description: {
	                  identifier  : 'description',
	                  rules: [
	                    {
	                      type   : 'empty',
	                      prompt : 'Please enter Description'
	                    },
	                    {
	                      type   : 'length[3]',
	                      prompt : 'Description must be at least 3 characters'
	                    }
	                  ]
	                },
	              }
	            })
	          ;
	        })
	    ;

	    $(".edit_customer").on("click", function() {
			$this = $(this);
			$("input[name='id']").val($this.data('id'));
			$("input[name='name']").val($this.data('name'));
			$("input[name='email']").val($this.data('email'));
			$("input[name='company']").val($this.data('company'));
			$("input[name='telephone']").val($this.data('telephone'));
			$("input[name='mobile']").val($this.data('mobile'));
			$('.ui.modal.customer').modal('show');
		});

		$(".submit").on("click", function() {
			$("form[name='myFormCustomer']").submit();
		});

		$(document)
	        .ready(function() {
	          $('#myFormCustomer')
	            .form({
	              fields: {
	              	name: {
	                  identifier  : 'name',
	                  rules: [
	                    {
	                      type   : 'empty',
	                      prompt : 'Please enter your Name'
	                    },
	                    {
	                      type   : 'length[3]',
	                      prompt : 'Your name must be at least 3 characters'
	                    }
	                  ]
	                },
	                email: {
	                  identifier  : 'email',
	                  rules: [
	                    {
	                      type   : 'empty',
	                      prompt : 'Please enter your e-mail'
	                    },
	                    {
	                      type   : 'email',
	                      prompt : 'Please enter a valid e-mail'
	                    }
	                  ]
	                }
	              }
	            })
	          ;
	        })
	    ;
	</script>
@stop
