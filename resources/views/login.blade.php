@extends('layouts.default')

@section('on-page-styles')
    <style type="text/css">
        body {
          background-color: #DADADA;
        }
        body .grid {
          height: 100%;
        }
        .image {
          margin-top: -100px;
        }
        .column {
          max-width: 450px;
        }
    </style>
@stop
@section('content')
    <div class="ui middle aligned center aligned grid">
        <div class="column">
        
            <h2 class="ui teal header">
                <div class="content">
                    UFEX Login
                </div>
            </h2>
            {!! Form::open(['route'=>'login', 'class'=>'ui large form']) !!}
                <div class="ui segment">
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" name="email" placeholder="E-mail address">
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" placeholder="Password">
                        </div>
                    </div>
                    <button type="submit" class="ui fluid large teal submit button">Login</button>
                </div>

                <div class="ui error message"></div>

                @if(Session::has('error'))
                <div class="ui negative message">
                    <i class="close icon"></i>
                    <div class="header">
                        Opps!
                    </div>
                    <p>{{ Session::get('error') }}</p>
                </div>
                @endif
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('on-page-scripts')

    <script>
      $(document)
        .ready(function() {
          $('.ui.form')
            .form({
              fields: {
                email: {
                  identifier  : 'email',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Please enter your e-mail'
                    },
                    {
                      type   : 'email',
                      prompt : 'Please enter a valid e-mail'
                    }
                  ]
                },
                password: {
                  identifier  : 'password',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Please enter your password'
                    },
                    {
                      type   : 'length[6]',
                      prompt : 'Your password must be at least 6 characters'
                    }
                  ]
                }
              }
            })
          ;
        })
      ;
      </script>
@stop
