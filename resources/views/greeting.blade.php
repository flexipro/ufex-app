@extends('layouts.default')

@section('on-page-styles')
	<style type="text/css">
		body {
			text-align: center;
		}
	</style>
@stop

@section('content')
	<p>The Universal Food Exchange app allows you to quickly and easily photograph, describe and provide a desired price for any excess foodthat you may have.</p>
	<p>This information is then sent to the brokerage team at the Universal Food Exchange who will work to source a buyer for your excess produce.</p>
	<p>Please continue to the app to start using all these features.</p>
@stop
