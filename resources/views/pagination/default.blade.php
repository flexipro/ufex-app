@if ($paginator->lastPage() > 1)
<div class="ui right floated pagination menu">
    <a href="{{ $paginator->url(1) }}" class="icon item {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
        <i class="left chevron icon"></i>
    </a>
    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <a href="{{ $paginator->url($i) }}" class="item {{ ($paginator->currentPage() == $i) ? ' active' : '' }}">{{ $i }}</a>
    @endfor
    <a href="{{ $paginator->url($paginator->currentPage()+1) }}" class="icon item {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
        <i class="right chevron icon"></i>
    </a>
</div>
@endif