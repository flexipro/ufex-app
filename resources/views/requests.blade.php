@extends('layouts.default')

@section('on-page-styles')
    <style type="text/css">
        body .grid {
          height: 100%;
        }
    </style>
@stop

@section('content')
	<h1>Requests</h1>
	<table class="ui celled table">
		<thead>
			<tr>
				<th>@sortablelink ('id', 'ID')</th>
				<th>@sortablelink ('name', 'Name')</th>
				<th>@sortablelink ('email', 'Email')</th>
				<th>@sortablelink ('company', 'Company')</th>
				<th>@sortablelink ('telephone', 'Telephone')</th>
				<th>@sortablelink ('mobile', 'Mobile')</th>
				<th>@sortablelink ('enquiry', 'Request')</th>
				<th width="150px">Action</th>
			</tr>
		</thead>
		<tbody>
		@foreach($enquiries as $enquiry)
			<tr>
				<td>{{ $enquiry->id }}</td>
				<td>{{ $enquiry->customer->name }}</td>
				<td>{{ $enquiry->customer->email }}</td>
				<td>{{ $enquiry->customer->company }}</td>
				<td>{{ $enquiry->customer->telephone }}</td>
				<td>{{ $enquiry->customer->mobile }}</td>
				<td>{{ $enquiry->enquiry }}</td>
				<td>
					<a href="{{ route('requests.delete', [$enquiry->id]) }}" data-method="delete" data-token="{{ csrf_token() }}" data-message="Are you sure you want to delete?" class="ui red button mini">Delete</a>
				</td>
			</tr>
		@endforeach

		@if($enquiries->count()==0)
			<tr><td colspan="7">No Data</td></tr>
		@endif
		</tbody>
	</table>
    
    @include('pagination.default', ['paginator' => $enquiries])

@stop

@section('on-page-scripts')
	
@stop
