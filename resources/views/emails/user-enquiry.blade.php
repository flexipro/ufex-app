<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>

		<p>Hello Brokerage Team,</p>

		<p>Customer ({{ $user->id }}) <strong>{{ $user->name }}</strong> has sent a purchase enquiry to UFEX. <a href="{{ env('SITE_URL') }}/requests/buy">Click here to view</a></p>

	</body>
</html>