<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>

		<p>Hello Brokerage Team,</p>

		<p>Customer ({{ $user->id }})<strong>{{ $user->name }}</strong> has deleted their post in UFEX. <a href="{{ env('SITE_URL') }}/customers/{{ $user->id }}/produce">Click here to view</a></p>

	</body>
</html>