<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>

		<p>Hello Brokerage Team,</p>

		<p>Customer ({{ $user->id }}) <strong>{{ $user->name }}</strong> is interested in the listing named "{{ $produce->name }}". <a href="{{ env('SITE_URL') }}/requests/produce">Click here to view</a></p>

	</body>
</html>