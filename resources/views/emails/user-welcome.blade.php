<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>

		<p>Hello, {{ $user->name }}:</p>

		<p>Thanks for registration!</p>
		<p>Click <a href="{{ url('api/activate/'.$user->id.'/code/'.$user->activation_code.'/') }}">here</a> to activate your account</p>
		<p>If url is not shown correctly, please copy this url <strong>{{ url('api/activate/'.$user->id.'/code/'.$user->activation_code.'/') }}</strong> and paste in your web browser.</p>
	</body>
</html>